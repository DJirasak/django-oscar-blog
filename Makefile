active:
	source venv/bin/activate

init:
	make migrate
	docker-compose -f docker/docker-compose.yml -p tutorial run --rm django python manage.py createsuperuser --noinput
	make start

start:
	docker-compose -f docker/docker-compose.yml -p tutorial up -d

restart:
	docker-compose -f docker/docker-compose.yml -p tutorial restart

makemigrations:
	docker-compose -f docker/docker-compose.yml -p tutorial run --rm django python manage.py makemigrations

migrate:
	docker-compose -f docker/docker-compose.yml -p tutorial run --rm django python manage.py migrate

shell:
	docker-compose -f docker/docker-compose.yml -p tutorial run --rm django python manage.py shell

clean:
	docker-compose -f docker/docker-compose.yml -p tutorial down --volumes --rmi all

bash:
	docker-compose -f docker/docker-compose.yml -p tutorial run --rm django bash

logs:
	docker-compose -f docker/docker-compose.yml -p tutorial logs -f django

test:
	docker-compose -f docker/docker-compose.yml -p tutorial run --rm django python manage.py test