from oscar.models.fields import AutoSlugField

from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class Category(models.Model):
    name = models.CharField(max_length=255, db_index=True)
    slug = AutoSlugField(populate_from="name", editable=True, max_length=255, unique=True, blank=True)

    def __str__(self):
        return self.name


class Post(models.Model):
    title = models.CharField(max_length=255, db_index=True)
    excerpt = models.TextField(max_length=10000, blank=True)
    content = models.TextField(max_length=50000, blank=True)
    featured_image = models.ImageField(upload_to='image', blank=True)
    date_created = models.DateTimeField(auto_now_add=True, db_index=True)
    date_updated = models.DateTimeField(auto_now=True, db_index=True)
    author = models.ForeignKey(
        User, on_delete=models.SET_NULL,
        null=True,
        limit_choices_to={'is_staff': True}
    )
    categories = models.ManyToManyField(Category, blank=True)
    slug = AutoSlugField(populate_from="title", editable=True, max_length=255, unique=True, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blogs:post-detail', args=[str(self.slug)])
