from django_filters.views import FilterView
from django_tables2 import SingleTableMixin, SingleTableView

from django.urls import reverse_lazy
from django.views import generic

from .filters import PostFilter
from .forms import CategoryForm, PostForm
from .tables import CategoryTable, PostTable
from ..models import Category, Post


class CategoryListView(SingleTableView):
    template_name = 'dashboard/category-list.html'
    model = Category
    table_class = CategoryTable
    context_table_name = 'category_table'


class CategoryCreateView(generic.CreateView):
    form_class = CategoryForm
    template_name = 'dashboard/category-form.html'
    success_url = reverse_lazy('dashboard_blog:category-list')
    extra_context = {'title': 'New Category', 'header': 'Add a new Category'}


class CategoryUpdateView(generic.UpdateView):
    model = Category
    form_class = CategoryForm
    template_name = 'dashboard/category-form.html'
    success_url = reverse_lazy('dashboard_blog:category-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = context['header'] = f'Edit Category "{self.object.name}"'
        return context


class CategoryDeleteView(generic.DeleteView):
    model = Category
    template_name = 'dashboard/category-delete.html'
    success_url = reverse_lazy('dashboard_blog:category-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = context['header'] = f'Delete Category "{self.object.name}"'
        return context


class PostListView(SingleTableMixin, FilterView):
    template_name = 'dashboard/post-list.html'
    table_class = PostTable
    context_table_name = 'posts_table'
    model = Post
    filterset_class = PostFilter


class PostCreateView(generic.CreateView):
    form_class = PostForm
    template_name = 'dashboard/post-form.html'
    success_url = reverse_lazy('dashboard_blog:post-list')
    extra_context = {'title': 'New Post', 'header': 'Add a new Post'}


class PostUpdateView(generic.UpdateView):
    model = Post
    form_class = PostForm
    template_name = 'dashboard/post-form.html'
    success_url = reverse_lazy('dashboard_blog:post-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = context['header'] = f'Edit Post "{self.object.title}"'
        return context


class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'dashboard/post-delete.html'
    success_url = reverse_lazy('dashboard_blog:post-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = context['header'] = f'Delete Post "{self.object.title}"'
        return context
