from django.urls import path

from . import views

app_name = 'dashboard_blog'
urlpatterns = [
    path('', views.PostListView.as_view(), name='post-list'),
    path('post/create', views.PostCreateView.as_view(), name='post-create'),
    path('post/<str:slug>/update', views.PostUpdateView.as_view(), name='post-update'),
    path('post/<str:slug>/delete', views.PostDeleteView.as_view(), name='post-delete'),

    path('category/', views.CategoryListView.as_view(), name='category-list'),
    path('category/create', views.CategoryCreateView.as_view(), name='category-create'),
    path('category/<str:slug>/update', views.CategoryUpdateView.as_view(), name='category-update'),
    path('category/<str:slug>/delete', views.CategoryDeleteView.as_view(), name='category-delete'),
]
