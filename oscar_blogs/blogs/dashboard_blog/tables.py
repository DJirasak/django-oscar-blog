from django_tables2 import DateTimeColumn, TemplateColumn
from oscar.core.loading import get_class

from ..models import Category, Post

DashboardTable = get_class('dashboard.tables', 'DashboardTable')


class PostTable(DashboardTable):
    date_created = DateTimeColumn(
        format='Y-m-d H:i'
    )
    date_updated = DateTimeColumn(
        format='Y-m-d H:i'
    )
    actions = TemplateColumn(
        template_name='dashboard/post-actions.html',
        orderable=False,
    )

    class Meta(DashboardTable.Meta):
        model = Post
        exclude = ('excerpt', 'content', 'featured_image')


class CategoryTable(DashboardTable):
    actions = TemplateColumn(
        template_name='dashboard/category-actions.html',
        orderable=False,
    )

    class Meta(DashboardTable.Meta):
        model = Category
