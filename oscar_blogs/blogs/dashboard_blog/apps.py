from oscar.core.application import OscarConfig


class DashboardBlogConfig(OscarConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'oscar_blogs.blogs.dashboard_blog'
    namespace = 'dashboard_blog'
