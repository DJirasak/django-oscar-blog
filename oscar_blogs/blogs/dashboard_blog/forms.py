from django import forms

from ..models import Category, Post


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            'title',
            'slug',
            'excerpt',
            'content',
            'featured_image',
            'categories',
            'author'
        ]


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name', 'slug']
