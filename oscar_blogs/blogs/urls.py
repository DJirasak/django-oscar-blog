from django.urls import path

from . import views

app_name = 'blogs'
urlpatterns = [
    path('', views.PostListView.as_view(), name='blogs-view'),
    path('category/<str:slug>', views.PostCategoryListView.as_view(), name='blogs-category-view'),
    path('post/<str:slug>/', views.PostDetailView.as_view(), name='post-detail'),
]
