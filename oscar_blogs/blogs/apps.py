from oscar.core.application import OscarConfig


class BlogsConfig(OscarConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'oscar_blogs.blogs'
