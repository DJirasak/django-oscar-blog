from django.contrib import admin

from .models import Category, Post


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ['name']


class PostAdmin(admin.ModelAdmin):
    search_fields = ['title', 'author']
    list_display = (
        'title',
        'author',
        'date_created',
        'date_updated',
        'slug'
    )
    list_filter = [
        'author',
        'date_created',
        'date_updated',
    ]
    fieldsets = [
        (None, {
            'fields': ['title', 'slug', 'excerpt'],
        }),
        ('Post', {
            'fields': ['content', 'featured_image', 'categories', 'author'],
        })
    ]


admin.site.register(Category, CategoryAdmin)
admin.site.register(Post, PostAdmin)
