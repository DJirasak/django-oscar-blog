from django_filters.views import FilterView
from django_tables2 import SingleTableMixin

from django.shortcuts import get_object_or_404
from django.views import generic

from .filters import PostFilter
from .models import Category, Post


class PostListView(SingleTableMixin, FilterView):
    model = Post
    template_name = 'blogs/post-list.html'
    extra_context = {'title': 'Posts', 'header': 'Recent Posts', 'categories': Category.objects.all()}
    context_object_name = 'posts'
    filterset_class = PostFilter
    ordering = ['-date_created']


class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'blogs/post-detail.html'


class PostCategoryListView(PostListView):
    extra_context = {'title': 'Posts', 'header': 'Category', 'categories': Category.objects.all()}

    def get_queryset(self):
        self.category = get_object_or_404(Category, slug=self.kwargs['slug'])
        return self.category.post_set.all()
